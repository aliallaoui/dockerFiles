Basic containers linked to make interaction possible between Nextcloud and J2EE webapp

Before using this docker-compose file, update your /etc/hosts and add following lines

127.0.0.1 tomcattest.docker
127.0.0.1 nextcloud.docker

Access to nextcloud and tomcat application using these domains