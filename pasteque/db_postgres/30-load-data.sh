#!/bin/bash
set -e

psql $DB_NAME -v ON_ERROR_STOP=1 -U $DB_USER -f /tmp/data_france.sql
